from flask import Flask, redirect, request, session, render_template, url_for, jsonify
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from google.oauth2.credentials import Credentials
import json
import os

app = Flask(__name__)
app.secret_key = os.getenv("FLASK_SECRET_KEY", "your_secret_key")

# Google API credentials
SCOPES = ["https://www.googleapis.com/auth/calendar", "openid", "https://www.googleapis.com/auth/userinfo.email"]

# Callback URL registered in Google Cloud Console
REDIRECT_URI = os.getenv("GOOGLE_REDIRECT_URI", "https://google.achrafsani.xyz/callback")

# Google API client setup
flow = InstalledAppFlow.from_client_secrets_file("client_secret.json", scopes=SCOPES, redirect_uri=REDIRECT_URI)

# Session keys
SIGN_UP_KEY = "sign_up"
CREDENTIALS_KEY = "credentials_file"
USER_INFO_KEY = "userinfo"

def get_flow():
    return flow

def authenticate_user():
    flow = get_flow()
    authorization_url, state = flow.authorization_url(prompt='consent')
    session['state'] = state
    return redirect(authorization_url)

def retrieve_events():
    try:
        credentials_data = session.get(CREDENTIALS_KEY)
        credentials = Credentials.from_authorized_user_info(credentials_data, SCOPES)
        service = build("calendar", "v3", credentials=credentials)

        events_result = service.events().list(calendarId='primary', maxResults=10, singleEvents=True, orderBy='startTime').execute()
        userinfo = build("oauth2", "v2", credentials=credentials).userinfo().get().execute()
        events = events_result.get('items', [])

        if not events:
            return 'No upcoming events found.'

        event_list = [f"{event['summary']} ({event['start']['dateTime']})" for event in events if "summary" in event.keys()]
        return jsonify({"data": event_list, "user": userinfo})

    except FileNotFoundError:
        return 'Token not found. Please authenticate first.'

@app.route("/sign_up")
def sign_up():
    session[SIGN_UP_KEY] = 1
    return authenticate_user()

@app.route("/login")
def login():
    session[SIGN_UP_KEY] = 0
    return authenticate_user()

@app.route("/callback")
def callback():
    if session[SIGN_UP_KEY]:
        session[SIGN_UP_KEY] = 0
        flow.fetch_token(authorization_response=request.url)
        session[CREDENTIALS_KEY] = flow.credentials.to_json()
        session[USER_INFO_KEY] = build("oauth2", "v2", credentials=session[CREDENTIALS_KEY]).userinfo().get().execute()

        if session[USER_INFO_KEY]["email"] in os.listdir("./"):
            return "2"

        with open(f'{session[USER_INFO_KEY]["email"]}_token.json', 'w') as token_file:
            token_file.write(session[CREDENTIALS_KEY])

        credentials = Credentials.from_authorized_user_info(session[CREDENTIALS_KEY], SCOPES)
        service = build("calendar", "v3", credentials=credentials)

        calendar = {
            'summary': 'achrafsani.xyz',
            'timeZone': 'America/Los_Angeles'
        }

        return jsonify({"response": "1"})

    else:
        flow.fetch_token(authorization_response=request.url)
        session[CREDENTIALS_KEY] = flow.credentials.to_json()
        session[USER_INFO_KEY] = build("oauth2", "v2", credentials=session[CREDENTIALS_KEY]).userinfo().get().execute()

    return redirect(url_for('events'))

@app.route("/events")
def events():
    return retrieve_events()

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
