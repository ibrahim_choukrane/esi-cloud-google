from flask import Flask, redirect, request, session, render_template
from google.oauth2 import id_token
from google_auth_oauthlib.flow import Flow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build

app = Flask(__name__)
app.secret_key = "your_secret_key"

# Google API credentials
CLIENT_ID = "1012521561714-r17gbn9r48uu8idn4a72rjf62s8u1g3l.apps.googleusercontent.com"
CLIENT_SECRET = "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"
SCOPES = ["https://www.googleapis.com/auth/calendar.events"]

# Callback URL registered in Google Cloud Console
REDIRECT_URI = "https://google.achrafsani.xyz/callback"

# Google API client setup
flow = Flow.from_client_secrets_file(
    "client_secret.json", scopes=SCOPES, redirect_uri=REDIRECT_URI
)

@app.route("/")
def index():
    authorization_url, state = flow.authorization_url(
        access_type="offline", include_granted_scopes="true"
    )
    session["state"] = state
    return render_template("login.html", authorization_url=authorization_url)

@app.route("/callback")
def callback():
    flow.fetch_token(authorization_response=request.url)
    credentials = flow.run_local_server(port=0)
    session["credentials"] = flow.credentials.to_json()
    #session["credentials"] = flow.credentials_to_dict()

    # Save the credentials for later use
    with open('token.json', 'w') as token_file:
        token_file.write(credentials.to_json())

    #if "token.json" in os.listdir("./"):

    # Use the credentials to access Google Calendar API
    service = build("calendar", "v3", credentials=flow.credentials)

    # Add an event to the user's calendar
    event = {
        "summary": "Sample Event",
        "description": "This is a sample event added via Google Calendar API.",
        "start": {"dateTime": "2024-01-05T10:00:00", "timeZone": "UTC"},
        "end": {"dateTime": "2024-01-05T11:00:00", "timeZone": "UTC"},
    }

    service.events().insert(calendarId="primary", body=event).execute()

    return "Event added to the calendar!"

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000)
