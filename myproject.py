
from flask import Flask, redirect, request, session, render_template, url_for, jsonify, flash
from google.oauth2 import id_token
from google_auth_oauthlib.flow import InstalledAppFlow, Flow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
from google.oauth2.credentials import Credentials
from datetime import datetime, timedelta
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
import requests
import json
import os

app = Flask(__name__)
app.secret_key = "your_secret_key"

# Google API credentials
SCOPES = "https://www.googleapis.com/auth/calendar openid https://www.googleapis.com/auth/userinfo.email"

# Callback URL registered in Google Cloud Console
REDIRECT_URI = "https://google.achrafsani.xyz/callback"

# Google API client setup
flow = InstalledAppFlow.from_client_secrets_file(
    "client_secret.json", scopes=SCOPES, redirect_uri=REDIRECT_URI
)

load_dotenv()
#app.secret_key = os.getenv('SECRET_KEY')
db_user = os.getenv('DB_USER')
db_password = os.getenv('DB_PASSWORD')
db_host = os.getenv('DB_HOST')
db_name = os.getenv('DB_NAME')
db_port = os.getenv('DB_PORT')

app.config['SQLALCHEMY_DATABASE_URI'] = (f"mysql+pymysql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}") 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

def invoke_email_function(args):
    url = 'https://faas-lon1-917a94a7.doserverless.co/api/v1/web/fn-e539efa2-934c-43fd-832b-e55fc2b9dd63/sample/emails'
    data = {
        'from': args.get("from"),
        'to': args.get("to"),
        'subject': args.get("subject"),
        'content': args.get("content")
    }
    headers = {
        'Content-Type': 'application/json',
    }
    response = requests.post(url, headers=headers, data=json.dumps(data))
    if response.status_code == 200:
        print("Email sent successfully")
        flash('Booking successful! Check your email for confirmation.')
    elif response.status_code == 400:
        flash('Failed to send email')
    else:
        flash('Booking successful! Check your email for confirmation.')
        print("Status Code:", response.status_code)
        print("Response:", response.text)









class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(200), nullable=False)

def authenticate_client(username, password):
    user = User.query.filter_by(username=username).first()
    if user :
        return True
    else:
        return False


def get_flow():
    return flow

def authenticate_user():
    flow = get_flow()
    authorization_url, state = flow.authorization_url(prompt='consent')
    session['state'] = state

    return redirect(authorization_url)

def retrieve_events():
    try:
        with open('elghazisoufiane02@gmail.com_token.json', 'r') as token_file:
            credentials_data = json.load(token_file)
            credentials = Credentials.from_authorized_user_info(credentials_data, SCOPES.split(" "))

        service = build("calendar", "v3", credentials=credentials)
        now = datetime.utcnow()
        time_min = now.isoformat() + 'Z'
        time_max = (now + timedelta(days=5)).isoformat() + 'Z'


        events_result = service.events().list(calendarId="aa9a45f9b13c759d61e2946a86b772a34d453a7bf59f487f516e92c611c31f54@group.calendar.google.com" , maxResults=100, timeMin=time_min, timeMax=time_max, singleEvents=True, orderBy='startTime').execute()
        #credentials = Credentials.from_authorized_user_info(credentials_data, SCOPES)
        userinfo = build("oauth2", "v2", credentials=credentials).userinfo().get().execute()
        events = events_result.get('items', [])
        if not events:
            return 'No upcoming events found.'

        #return redirect(url_for('target_page'), code=307, response=jsonify(json_data))

        #event_list = [{'event': event["summary"], 'start_time': event["start"]["dateTime"], 'end_time': event["end"]["dateTime"]} for event in events if "summary" in event.keys()]

        event_list = [f'event: {event["summary"]}, start_time: {event["start"]["dateTime"]}, end_time: {event["end"]["dateTime"]}' for event in events if "summary" in event.keys()]
        data = f"data : {event_list}"
        #json_data_str = jsonify({"data": event_list, "provider_calendar_id": session["calendar_id_provider"]}).data.decode('utf-8')
        provider = f'provider_calendar_id: {session["calendar_id_provider"]}'

        #return redirect(f'https://book-esi-axju6.ondigitalocean.app/get_client_information?json_data={data + ", " + provider}')
        return redirect(f'https://book-esi-axju6.ondigitalocean.app/get_client_information?calendar_id={session["calendar_id_provider"]}&username={session["username"]}')

        #return jsonify({"data": event_list, "provider_calendar_id": session["calendar_id_provider"]})

    except FileNotFoundError:
        return 'Token not found. Please authenticate first.'

@app.route("/sign_up")
def sign_up():
    session["sign_up"] = 1
    return authenticate_user()

def index():
    return render_template("login.html")

@app.route("/login")
def login():
    session["sign_up"] = 0
    return authenticate_user()

@app.route("/callback")
def callback():
    if session["sign_up"]:
        session["sign_up"] = 0
        flow.fetch_token(authorization_response=request.url)
        session["credentials"] = flow.credentials.to_json()
        session["userinfo"] = build("oauth2", "v2", credentials=flow.credentials).userinfo().get().execute()
        if f'{session["userinfo"]["email"]}_token.json' in os.listdir("./"):
            return jsonify({"response": "2"})

        with open(f'{session["userinfo"]["email"]}_token.json', 'w') as token_file:
            token_file.write(session["credentials"])

        #credentials = Credentials.from_authorized_user_info(session['credentials'], SCOPES)
        service = build("calendar", "v3", credentials=flow.credentials)

        calendar = {
        'summary': 'achrafsani.xyz_client',
        'timeZone': 'Africa/Casablanca'
        }
        client_calendar = service.calendars().insert(body=calendar).execute()

        calendar = {
        'summary': 'achrafsani.xyz_provider',
        'timeZone': 'Africa/Casablanca'
        }
        provider_calendar = service.calendars().insert(body=calendar).execute()

        #return jsonify({"response": "1", "client calandar id": client_calendar["id"], "provider calandar id": provider_calendar["id"]})
        return redirect(f"https://book-esi-axju6.ondigitalocean.app/get_information")
    else :
        flow.fetch_token(authorization_response=request.url)
        credentials = flow.credentials
        session["credentials"] = credentials.to_json()
        session["userinfo"] = build("oauth2", "v2", credentials=credentials).userinfo().get().execute()
        service = build("calendar", "v3", credentials=credentials)
        page_token = None
        while True:
            calendar_list = service.calendarList().list(pageToken=page_token).execute()
            for calendar_list_entry in calendar_list['items']:
                if calendar_list_entry['summary'] == "achrafsani.xyz_client":
                    session["calendar_id_client"] = calendar_list_entry['id']

                if calendar_list_entry['summary'] == "achrafsani.xyz_provider":
                    session["calendar_id_provider"] = calendar_list_entry['id']

            page_token = calendar_list.get('nextPageToken')
            if not page_token:
                break
        return redirect(url_for('events'))

@app.route("/events")
def events():
    return retrieve_events()

@app.route("/create_event")
def create_event():

    #title = name = request.args.get('title', 'Guest')
    #start_date = request.args.get('start_date', '2024-01-08T11:00:00')
    #end_date = request.args.get('end_date', '2024-01-08T12:00:00')
    date = request.args.get('selectedDate', '2024-01-08T11:00:00')
    time = request.args.get('selectedTime' ,'2024-01-08T11:00:00')
    datetime_string = f"{date}T{time}:00"

    with open('elghazisoufiane02@gmail.com_token.json', 'r') as token_file:
        credentials_data = json.load(token_file)
        credentials = Credentials.from_authorized_user_info(credentials_data, SCOPES.split(" "))

    service = build("calendar", "v3", credentials=credentials)
    page_token = None
    while True:
        calendar_list = service.calendarList().list(pageToken=page_token).execute()
        for calendar_list_entry in calendar_list['items']:
            if calendar_list_entry['summary'] == "achrafsani.xyz_client":
                session["calendar_id_client"] = calendar_list_entry['id']

            if calendar_list_entry['summary'] == "achrafsani.xyz_provider":
                session["calendar_id_provider"] = calendar_list_entry['id']

        page_token = calendar_list.get('nextPageToken')
        if not page_token:
            break

    service = build("calendar", "v3", credentials=credentials)
    calender_id = request.args.get('calender_id', session["calendar_id_provider"])
    event = {
        'summary': "test title",
        'start': {
            'dateTime': datetime_string,
            'timeZone': 'Africa/Casablanca'
        },
        'end': {
            'dateTime': datetime_string,
            'timeZone': 'Africa/Casablanca',
        },
    }

    created_event = service.events().insert(calendarId="aa9a45f9b13c759d61e2946a86b772a34d453a7bf59f487f516e92c611c31f54@group.calendar.google.com", body=event).execute()

    user_email = request.args.get('user_email')
    email_params = {
        "from": "contact@achrafsani.xyz",
        "to": user_email,
        "subject": "Your Booking Confirmation",
        "content": "Thank you for booking with us! Your appointment is confirmed."
    }
    invoke_email_function(email_params)

    service = build("calendar", "v3", credentials=credentials)
    page_token = None

    return redirect(url_for('events'))



@app.route('/login_client', methods=['GET', 'POST'])
def login_client():
    if request.method == 'POST':
        if authenticate_client(request.form['username'], request.form['password']):
            session["username"] = request.form["username"]
            return redirect(f'https://book-esi-axju6.ondigitalocean.app/get_client_information?username={request.form["username"]}')
        else:
            return redirect(f'https://book-esi-axju6.ondigitalocean.app')
    else:
        # Handle GET request, render login page
        return redirect(f'https://book-esi-axju6.ondigitalocean.app')



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
